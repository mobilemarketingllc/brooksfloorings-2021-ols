<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/resources/slick/script.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
});


//$500 off coupon form code sharpspring
add_action( 'gform_after_submission_7', 'post_to_third_party_7', 10, 2 );

function post_to_third_party_7( $entry, $form ) {
    $baseURI = 'https://app-3QNH93QVYY.marketingautomation.services/webforms/receivePostback/MzawMDE3MjM1BQA/';
    $endpoint = '171b13e6-8548-49be-9cd1-02749211aa25';
    $post_url = $baseURI . $endpoint;

    $field_id = 8; $field = GFFormsModel::get_field( $form, $field_id );
    $field_value_8 = is_object( $field ) ? $field->get_value_export( $entry ) : '';

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '7' ),
        'terms conditions' => $field_value_8,
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//Contact us form code sharpspring
add_action( 'gform_after_submission_6', 'post_to_third_party_6', 10, 2 );

function post_to_third_party_6( $entry, $form ) {
    $baseURI = 'https://app-3QNH93QVYY.marketingautomation.services/webforms/receivePostback/MzawMDE3MjM1BQA/';
    $endpoint = '09539c4f-0d0b-427c-bd8c-cb870529d94d';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '7' ),
        'questions comments' => rgar( $entry, '9' ),
        'terms conditions' => rgar( $entry, '8' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//Schedule an appointme form code sharpspring
add_action( 'gform_after_submission_3', 'post_to_third_party_3', 10, 2 );

function post_to_third_party_3( $entry, $form ) {
    $baseURI = 'https://app-3QNH93QVYY.marketingautomation.services/webforms/receivePostback/MzawMDE3MjM1BQA/';
    $endpoint = 'be312028-2a22-4d4c-92c8-60f46d3cd668';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'phone' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '4' ),
        'product choice' => rgar( $entry, '16' ),
        'preferred time' => rgar( $entry, '15' ),
        'preferred date' => rgar( $entry, '13' ),
        'terms conditions' => rgar( $entry, '12' ),
        'url' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}



// Actions
//add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 ); 

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
remove_action( 'wp_head', 'feed_links_extra', 3 );


function new_google_keyword() 
 {
	$keyword = $_GET['keyword'];
	$brand = $_GET['brand'];   
	if( $keyword ==""  && $brand == "")
    {	   
        return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON FLOORING*<h1>';
    }
    else
    {
         return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON '.$brand.' '.$keyword.'<h1>';
    }
 }
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	 /* var brand_val ='<?php echo $_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo $_COOKIE['keyword'];?>';  */
	  
	  var brand_val ='<?php echo $brand;?>';
	  var keyword_val = '<?php echo $keyword;?>'; 

      jQuery(document).ready(function($) {
      jQuery("#input_7_10").val(keyword_val);
      jQuery("#input_7_11").val(brand_val);
    });
  </script>
  <?php  
  /*   setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600); */
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;            
           }
      </style>  
   <?php    
}
function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');

// Remove query string from static content
function _remove_script_version( $src ){ 
$parts = explode( '?', $src ); 	
return $parts[0]; 
} 
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );



// module enqueued google fonts
add_filter( 'fl_builder_google_fonts_pre_enqueue', function( $fonts ) {
    return array();
    return $fonts;
} );

// takes care of theme enqueues
add_action( 'wp_enqueue_scripts', function() {
    global $wp_styles;
    if ( isset( $wp_styles->queue ) ) {
        foreach ( $wp_styles->queue as $key => $handle ) {
            if ( false !== strpos( $handle, 'fl-builder-google-fonts-' ) ) {
                unset( $wp_styles->queue[ $key ] );
            }
        }
    }
}, 101 );

//Yoast SEO Breadcrumb link - Changes for PDP pages
//add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );
/*
function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/',
            'text' => 'Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/carpet/',
            'text' => 'Carpeting',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/carpet/products/',
            'text' => 'Carpeting Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'hardwood_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/',
            'text' => 'Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/hardwood/',
            'text' => 'Hardwood Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/hardwood/products/',
            'text' => 'Hardwood Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'laminate_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/',
            'text' => 'Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/laminate/',
            'text' => 'Laminate Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/laminate/products/',
            'text' => 'Laminate Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'tile_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/',
            'text' => 'Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/tile/',
            'text' => 'Tile Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/tile/products/',
            'text' => 'Tile Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'luxury_vinyl_tile' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/',
            'text' => 'Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/luxury-vinyl/',
            'text' => 'Tile Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/luxury-vinyl/products/',
            'text' => 'Tile Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'solid_wpc_waterproof' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/',
            'text' => 'Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/waterproof/',
            'text' => 'waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/floors/flooring/waterproof/products/',
            'text' => 'Waterproof Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    return $links;
}*/

add_action( 'wp_enqueue_scripts', 'misha_main_theme_css' );
function misha_main_theme_css() {
    if(is_singular( 'carpeting' ) || is_singular( 'hardwood_catalog' ) || is_singular( 'laminate_catalog' ) || is_singular( 'luxury_vinyl_tile' ) || is_singular( 'tile_catalog' )) {
?>
	<style type="text/css">
        #breadcrumbs{
            background: #003f58 !important;
        }
        #breadcrumbs + .container{
            padding-top: 90px !important;
        }
    </style>
<?php 
    }
}
//add method to register event to WordPress init
add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   
}
add_filter( 'auto_update_plugin', '__return_false' );